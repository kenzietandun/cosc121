#!/usr/bin/env python3

from tkinter import *
from tkinter.ttk import *

TEMPLATE = "Name: {0}\nHeight: {1:.2f} m\nHorns: {2}"

class Blork(object):
    """Defines the Blork class.
    Data attributes: name of type str
                     height (metres) of type float
                     has_horns of type bool
    """

    def __init__(self, name, height, has_horns=False):
        """Blork constructor"""
        self.name = name
        self.height = height
        self.has_horns = has_horns


class BlorkGui(object):
    """Defines the Blork Interface"""

    def __init__(self, window, blorks):
        """Setup the label and button on given window"""
        self.blorks = blorks

        # Combo Box
        # Your combo box code goes here
        self.blork_box = Combobox(window, 
                values=list(self.blorks.keys()))
        self.blork_box.grid(row=0, column=0)

        # View Button
        # Your button code goes here
        self.details_button = Button(window,
                command=self.view_blork,
                text="View details")
        self.details_button.grid(row=0, column=1)

        # Details Label
        # Your label code goes here
        self.details_label = Label(window, 
                text="Press 'View details'")
        self.details_label.grid(row=1, column=0)

    # Your button function goes here
    def view_blork(self):
        """Show selected blork in details_label"""
        selected_blork = self.blorks[self.blork_box.get()]
        message = TEMPLATE.format(
                selected_blork.name,
                selected_blork.height,
                "Yes" if selected_blork.has_horns else "No")
        self.details_label["text"] = message

def main():
    """Set up the GUI and run it."""
    blorks = {"Jeff": Blork("Jeff", 1.6),
              "Lily": Blork("Lily", 1.111111),
              "Jack": Blork("Jack", 1.89),
              "Chewblorka": Blork("Chewblorka", 3.14, True),
              "Blorkstien": Blork("Blorkstien", 0.856, True)}
    window = Tk()
    blork_gui = BlorkGui(window, blorks)
    window.mainloop()

main()
