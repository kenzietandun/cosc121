#!/usr/bin/env python3

class Whosit:
    """
    Defines the Whosit class.
    Data attributes: unit_type of type str
                     name of type str
                     health of type float
    """

    def __init__(self, unit_type, name, health):
        """Whosit constructor"""
        if unit_type not in ['Greebling', 'Throve', 'Plaguelet']:
            raise ValueError('Invalid Whosit type')
        self.unit_type = unit_type
        self.name = name
        self.health = health
        if self.health < 0:
            self.health = 0

    def __str__(self):
        """Returns a formatted string represent of object Whosit"""
        name = self.name
        unit_type = self.unit_type
        health = self.health
        template = "{0} ({1}), health = {2:.1f}".format(name, unit_type, health)
        return template

    def suffer(self, damage):
        """Reduces Whosit health by <<damage>> amount, 
        Whosit's health will not fall below 0"""
        self.health -= damage
        if self.health < 0:
            self.health = 0
