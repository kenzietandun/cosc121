#!/usr/bin/env python3

class Blork:
    """Defines the Blork class.
    Data attributes: name of type str
                     height (metres) of type float
                     has_horns of type bool
    """

    def __init__(self, name, height, has_horns=False, baranges=0):
        """Blork constructor"""
        self.name = name
        self.height = height
        self.has_horns = has_horns
        self.baranges = baranges

    def say_hello(self):
        """Blork says hello, if it has horns, it will
        print its greeting in capitals"""
        greeting = "Hi! My name is {}!".format(self.name)
        if self.has_horns:
            greeting = greeting.upper()

        print(greeting)

    def __str__(self):
        """Returns the formatted string represent of the Blork object"""
        name = self.name
        height = self.height
        template = "{0} is a {1:.2f} m tall".format(name, height)

        if self.has_horns:
            template += " horned blork!"
        else:
            template += " blork!"

        return template
            

    def collect_baranges(self, number=1):
        """Adds baranges to Blork's total baranges by <<number>>"""
        self.baranges += number

    def eat(self):
        """Blork eats the one of the barange(s), increasing his height by
        0.1m. If there's no barange in stock, he will inform you"""
        if self.baranges > 0:
            self.baranges -= 1
            self.height += 0.1
        else:
            print("I don't have any baranges to eat!")

    def feast(self):
        """Blork goes on a feast. Grows horns if it didn't have one before.
        If it had a horn, it grows by 50% in height. Blork will tell you
        if it doesn't have enough baranges for a feast."""
        if self.baranges >= 5:
            if not self.has_horns:
                self.has_horns = True
            else:
                self.height = 1.5 * self.height
            self.baranges -= 5
        else:
            print("I don't have enough baranges to feast!")
