#!/usr/bin/env python3

class Thingy:
    """
    Defines the Thingy class.
    Data attributes: name of type str
                     height of type int
                     words of type list
    """

    def __init__(self, name, height, words):
        """Thingy contructor"""
        self.name = name
        self.height = height
        self.words = words
    
    def __str__(self):
        """Returns a formatted string represent of object Thingy"""
        name = self.name
        height = self.height
        words = self.words
        template = "Hi. I'm {0}. I'm {1} cm tall.\n".format(
                name, height)
        template += "Words I know: {0}.".format(', '.join(words))
        return template

    def taller_by(self, number):
        """Increases Thingy's height by <<number>>"""
        self.height += number

    def learn(self, word_list):
        """Adds <<word_list>> to the list of words Thingy knows"""
        self.words.extend(word_list)
