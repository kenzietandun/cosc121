#!/usr/bin/env python3

class Person:
    """Defines a Person class, suitable for use in a hospital contect.
    Data attributes: name of type str
                     age of type int
                     weight(kg) of type float
                     height(metres) of type float
    Methods: bmi()
    """

    def __init__(self, name, age, weight, height):
        """Creates a new Person object with the specified name, age, weight
        and height"""
        self.name = name
        self.age = age
        self.weight = weight
        self.height = height

    def bmi(self):
        """Returns the body mass index of the person"""
        return self.weight / (self.height * self.height)

    def status(self):
        """Returns a person's status based on their body mass index"""
        bmi_measurement = self.bmi()
        if bmi_measurement < 18.5:
            status = "Underweight"
        elif bmi_measurement < 25:
            status = "Normal"
        elif bmi_measurement < 30:
            status = "Overweight"
        else:
            status = "Obese"
        return status

    def __str__(self):
        """Returns the formatted string represent of the Person object"""
        name = self.name
        age = self.age
        bmi = self.bmi()
        status = self.status()
        template = "{0} ({1}) has a bmi of {2:3.2f}. Their status is {3}."
        return template.format(name, age, bmi, status)

def read_people(csv_filename):
    """Reads data from <<csv_filename>>, then returns a list of people
    from the file"""
    peoples = []
    with open(csv_filename, 'r') as csv:
        for line in csv:
            name, age, weight, height = line.strip().split(',')
            age = int(age)
            weight = float(weight)
            height = float(height)
            peoples.append(Person(name, age, weight, height))

    return peoples

def filter_people(people_list, status):
    """Returns a list of people with status <<status>>"""
    return [people for people in people_list if people.status() == status]
