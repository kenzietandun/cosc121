#!/usr/bin/env python3

class Car:
    """
    Defines the Car class.
    Data attributes: model of type str
                     year of type int
                     speed of type int
    """

    def __init__(self, model, year, speed=0):
        """Car constructor"""
        self.model = model
        self.year = year
        self.speed = speed

    def accelerate(self):
        """Car accelerates, adds 5km/h to its speed"""
        self.speed += 5

    def brake(self):
        """Car brakes, reduces 5km/h of its speed
        whenever possible"""
        if self.speed < 5:
            self.speed = 0
        else:
            self.speed -= 5

    def honk_horn(self):
        """GET OUTTA THE WAY!"""
        print("{} goes 'beep beep'".format(self.model))

    def __str__(self):
        """Returns a formatted string represent of Car object"""
        model = self.model
        year = self.year
        speed = self.speed
        template = "{0} ({1}), moving at {2} km/h.".format(model, year, speed)
        return template
