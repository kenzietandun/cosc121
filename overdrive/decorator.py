#!/usr/bin/env python3

from random import randint

# Initialise array w/ random ints
array = []
total_n = 10
for i in range(total_n):
    row = []
    for j in range(total_n):
        row.append(randint(1, 9))
    array.append(row)

# Print array
i = len(array) - 1
for row in reversed(array):
    print("{}".format(row))
    i -= 1

class memoise:
    def __init__(self, f):
        self.f = f
        self.cache = {}

    def __call__(self, *args, **kwargs):
        position = (kwargs["row"], kwargs["column"])
        if position in self.cache:
            return self.cache[position]
        else:
            self.cache[position] = result = self.f(*args, **kwargs)
        return result

@memoise
def find_path_recursion(array, row=0, column=0):
    if row == 0:
        return array[row][column]
    elif column >= len(array) - 1: # right most column
        min_position = min([find_path_recursion(array, row=row-1, column=column-1),
                            find_path_recursion(array, row=row-1, column=column)])
    elif column <= 0: # left most column
        min_position = min([find_path_recursion(array, row=row-1, column=column),
                            find_path_recursion(array, row=row-1, column=column+1)])
    else: # not on the column edge
        min_position = min([find_path_recursion(array, row=row-1, column=column-1),
                            find_path_recursion(array, row=row-1, column=column),
                            find_path_recursion(array, row=row-1, column=column+1)])
    result = array[row][column] + min_position
    return result

#def find_path_iteration(array, row=0, column=0):

array_length = len(array) - 1
paths = []
for col in range(0, len(array)):
    paths.append(find_path_recursion(array, row=array_length, column=col))

print(min(paths))
