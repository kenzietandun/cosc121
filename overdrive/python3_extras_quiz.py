#!/usr/bin/env python3

import itertools
import functools

@functools.lru_cache(maxsize=None)
def fib(n):
    return 1 if n < 2 else fib(n-1) + fib(n-2)

def words_in_string(s):
    return (word for word in s.split())
#    for word in s.split():
#        yield word

#generator = words_in_string('Wee Willie Winkie')
#print(type(generator))
#try:
#    while True:
#        print(generator.__next__())
#except StopIteration:
#    print("That worked!")
#except Exception as e:
#    print("Oops, got", e)



#words = list(words_in_string('blah blah black sheep'))
#print(words)

class Point:
    def __init__(self, x, y):
        self.coords = [x, y]

    @property
    def x(self):
        return self.coords[0]

    @property
    def y(self):
        return self.coords[1]

def sqr(f):
    """decorator"""
    def new_f(*args):
        return f(*args) ** 2
    return new_f

@sqr
def blah(x):
    return x

#func_call_counts = {}
#def profile(f):
#    """count the number of times function f is called"""
#    function_name = f.__name__
#    def new_f(*args):
#        func_call_counts[f.__name__] = func_call_counts.get(f.__name__, 0) + 1
#        return f(*args)
#    return new_f

class profile:
    func_call_counts = {}
    def __init__(self, f):
        """profile constructor"""
        self.f = f

    def __call__(self, *args):
        self.func_call_counts[self.f.__name__] = \
            self.func_call_counts.get(self.f.__name__, 0) + 1
        return self.f(*args)

@profile
def f1(n):
    return n

@profile
def f2(n):
    for i in range(n):
        print(f1(i))

f2(5)
print("Profile results:")
print(sorted(profile.func_call_counts.items()))






















def group_by_date(records):
    return [list(value) for _, value in itertools.groupby(records, key=lambda x: x[0])]

def print_permutations(s):
    """fun"""
    print('\n'.join(sorted([''.join(perm) for perm in itertools.permutations(s, len(s))])))

def print_possible_words(word):
    """fun"""
    [print(''.join(comb)) for comb in sorted(list(set([combi for combi in itertools.permutations(word, len(word))])))]
