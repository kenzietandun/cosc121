#!/usr/bin/env python3

def squares(n):
    """generates the sequence i**2 from 1 to n incl. """
    for i in range(1, n + 1):
        yield(i**2)

for i in squares(6):
    print(i)
