#!/usr/bin/env python3

import symbols

class Parser:
    def __init__(self, lines):
        """constructor for parser object
        params: List of String of hack code lines"""
        self.lines = lines
        self.lines = self.remove_comments()
        self.parse_labels()
    
    def parse_labels(self):
        """parse for possible labels and store
        them inside a dictionary of on which line
        they are located"""
        self.labels = {}
        line_count = 0
        for line in self.lines:
            if line.startswith('('):
                # a label
                label_name = line[1:-1]
                self.labels[label_name] = line_count
            else:
                line_count += 1

    def remove_comments(self):
        """remove pesky comments from code"""
        lines = []
        for line in self.lines:
            if not line or line.startswith("//"):
                continue
            elif ' ' in line:
                cmd = line.split(' ')[0]
                lines.append(cmd)
            else:
                lines.append(line)
        return lines

    def parse_a_inst(self, line):
        """parses an A-instruction line"""
        a_reg = line[1:]
        if not a_reg.isdigit():
            a_reg = symbols.predefined.get(a_reg,
                    a_reg)
            a_reg = self.labels.get(a_reg, a_reg)
            a_reg = str(a_reg)
            if not a_reg.isdigit():
                memory_location = symbols.curr_memory_location - \
                        symbols.predefined_length + \
                        symbols.memory_start_location
                symbols.predefined[a_reg] = memory_location
                symbols.curr_memory_location += 1
                a_reg = memory_location
        a_reg = bin(int(a_reg))
        a_reg = '0' + a_reg[2:].zfill(15)
        return a_reg

    def parse_c_inst(self, line):
        """parses a C-instruction line"""
        template = "111{a}{comp}{dest}{jump}"

        a = 0
        if '=' in line and ';' in line:
            if 'M' in t_comp:
                a = 1
            t_dest, cmd_jump = line.split('=')
            t_comp, t_jump = cmd_jump.split(';')
        elif ';' in line:
            t_comp, t_jump = line.split(';')
            t_dest = symbols.dest["null"]
            t_comp = symbols.comp[t_comp]
            t_jump = symbols.jump[t_jump]
        elif '=' in line:
            t_dest, t_comp = line.split('=')
            if 'M' in t_comp:
                a = 1
            t_dest = symbols.dest[t_dest]
            t_comp = symbols.comp[t_comp]
            t_jump = symbols.jump["null"]
        return template.format(
                    comp=t_comp,
                    dest=t_dest,
                    jump=t_jump, a=a)

    def parse(self):
        """parses lines of asm codes into hack binaries"""
        result = []
        for line in self.lines:
            if line.startswith('@'):
                # A instruction
                a_inst = self.parse_a_inst(line)
                result.append(a_inst)
            elif line.startswith('('):
                continue
            else:
                # C instruction
                c_inst = self.parse_c_inst(line)
                result.append(c_inst)

        return result
