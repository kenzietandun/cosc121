#!/usr/bin/env python3

from parser import Parser

def main():
    """main function"""
    with open("pong/Pong.asm", 'r') as fil:
        lines = fil.readlines()

    lines = [l.strip() for l in lines]

    parser = Parser(lines)

    with open("pong/test.hack", 'w') as fil:
        for line in parser.parse():
            fil.write(line + '\n')

if __name__ == "__main__":
    main()
