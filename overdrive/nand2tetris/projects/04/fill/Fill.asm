(START)
    @16384
    D=A
    @start
    M=D

    @24576
    D=A
    @fin
    M=D
(ENDSTART)

(CLEAN)
    @start
    D=M
    @fin
    D=D-M
    @ENDCLEAN
    D;JEQ
    @start
    A=M
    M=0
    @start
    M=M+1
    @CLEAN
    0;JMP
(ENDCLEAN)

(KEEB)
    @fin
    A=M
    D=M
    @ENDKEEB
    D;JNE
    @START
    D;JEQ
    @KEEB
    0;JMP
(ENDKEEB)

@16384
D=A
@start
M=D

(PAINT)
    @start
    D=M
    @fin
    D=D-M
    @KEEB
    D;JEQ
    @start
    A=M
    M=-1
    @start
    M=M+1
    @PAINT
    0;JMP
(ENDPAINT)

@START
0;JMP
