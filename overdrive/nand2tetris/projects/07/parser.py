#!/usr/bin/env python3

command_types = {
        "pop"      : "C_POP",
        "push"     : "C_PUSH",
        "label"    : "C_LABEL",
        "goto"     : "C_GOTO",
        "if"       : "C_IF",
        "function" : "C_FUNCTION",
        "call"     : "C_CALL",
        "return"   : "C_RETURN",
        }

class Parser:
    def __init__(self, lines):
        """constructor for Parser"""
        self.lines = lines
        self.pointer = -1

    def has_more_commands(self):
        """checks if there are more commands"""
        return self.pointer <= len(self.lines) - 2

    def advance(self):
        """advances through the commands only if there
        are more commands"""
        self.pointer += 1
        self.current_command = self.lines[self.pointer]

    def command_type(self):
        """returns the type of current command"""
        curr_cmd = self.current_command.split()[0]
        curr_cmd_type = command_types.get(curr_cmd,
                "C_ARITHMETIC")
        return curr_cmd_type

    def arg1(self):
        """returns the first argument of the current command
        if command type is C_ARITHMETIC, return the command itself
        should not be called for C_RETURN"""

        curr_cmd = self.current_command
        if self.command_type() == "C_ARITHMETIC":
            return curr_cmd.split()[0]
        else:
            return curr_cmd.split()[1]

    def arg2(self):
        """returns the second argument if the current command
        only works for C_PUSH, C_POP, C_FUNCTION, C_CALL"""

        curr_cmd = self.current_command
        return curr_cmd.split()[2]
