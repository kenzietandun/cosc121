#!/usr/bin/env python3

import sys
import os
import parser
import code_writer

def get_file_lines(vm_file):
    """get lines from a .vm file
    removing comments and empty lines

    Returns: list of lines"""

    with open(vm_file, 'r') as vm:
        lines = [line.strip() for line in vm
                if not line.startswith("//")]
        lines = list(filter(None, lines))
    return lines

def main():
    """main function"""

    arg = sys.argv[1]
    if arg.endswith(".vm"):
        vm_file = arg
        output_file = arg.replace(".vm", ".asm")
        writer = code_writer.CodeWriter(output_file)
        lines = get_file_lines(vm_file)
        psr = parser.Parser(lines)
        while psr.has_more_commands():
            psr.advance()
            cmd_type = psr.command_type()
            segment = psr.arg1()
            if cmd_type != "C_ARITHMETIC":
                index = psr.arg2()
                writer.write_push_pop(cmd_type, segment, index)
            else:
                writer.write_arithmetic(segment)
    else:
        vm_dir = arg
        vm_files = [vm_dir + '/' + fil for fil in os.listdir(arg)
                if fil.endswith(".vm")]
        for vm_file in vm_files:
            lines = get_file_lines(vm_file)

if __name__ == "__main__":
    main()
