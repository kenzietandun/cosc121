#!/usr/bin/env python3

import os

class CodeWriter:
    def __init__(self, output_file):
        """constructor for CodeWriter"""
        self.output_file = output_file
        filename = output_file.split('/')[-1]
        self.filename = filename.split('.')[0]
        if not os.path.isfile(output_file):
            os.mknod(self.output_file)
        self.out_file = open(self.output_file, 'w')
        self.label_counter = 1

        self.init_temp_pointer()
        
        self.out_file.write("@256\n" # base addr SP
                            "D=A\n"
                            "@0\n"
                            "M=D\n"
                            "@300\n" # base addr LCL
                            "D=A\n"
                            "@1\n"
                            "M=D\n"
                            "@400\n" # base addr argument
                            "D=A\n"
                            "@2\n"
                            "M=D\n"
                            "@3000\n" # base addr this
                            "D=A\n"
                            "@3\n"
                            "M=D\n"
                            "@3010\n" # base addr that
                            "D=A\n"
                            "@4\n"
                            "M=D\n"
                            )

    def write_init(self):
        """sets the stack pointer to position 256
        and call sys.init
        at the beginning of the file"""
        self.out_file.write("@256\n"
                            "D=A\n"
                            "@SP\n"
                            "M=D\n" # set SP value
                            )

    def write_label(self, label):
        """writes label"""
        self.out_file.write("({label})\n".format(
                                label=label))

    def write_goto(self, label):
        """writes goto label command"""
        self.out_file.write("@{label}\n"
                            "0;JMP\n".format(
                                label=label))

    def write_if(self, label):
        """writes if command
        only jump to label if value at SP isn't 0"""
        self.out_file.write("@SP\n"
                            "M=M-1\n"
                            "@SP\n"
                            "A=M\n"
                            "D=M\n"
                            "@{label}\n"
                            "D;JNE\n"
                            .format(label=label))
    
    def write_function(self, function_name, num_locals):
        """writes the assembly code that is the 
        translation of the given funciton command"""
        self.out_file.write("({func_name})\n".format(
            func_name=function_name))
        for i in range(num_locals):
            self.write_push_pop("C_PUSH", "constant", "0")

    def write_return(self):
        """writes the assembly code that is the translation
        of the given return command"""
        self.out_file.write("@LCL\n"
                            "D=M\n"
                            "@FRAME\n"
                            "M=D\n" # FRAME=LCL
                            "@FRAME\n"
                            "D=M\n"
                            "D=D-1\n"
                            "D=D-1\n"
                            "D=D-1\n"
                            "D=D-1\n"
                            "D=D-1\n"
                            "@RET\n"
                            "M=D\n" # RET=*FRAME-5
                            )





    def write_arithmetic(self, command):
        """writes the assembly code that is
        the translation of the given arithmetic method"""

        # Get the top two values in the stack
        # Topmost value can be accessed with D
        # Second topmost value can be accessed with M
        get_top_two_stack = ("@SP\n"
                             "M=M-1\n"
                             "A=M\n"
                             "D=M\n"
                             "M=0\n"
                             "@SP\n"
                             "M=M-1\n"
                             "A=M\n")

        # Get the topmost value in the stack
        # Value can be accessed with D
        get_top_stack = ("@SP\n"
                         "M=M-1\n"
                         "A=M\n"
                         "D=M\n")

        # Decrease the stack pointer by 1
        dec_stack_pointer = ("@SP\n"
                             "M=M-1\n")

        # Increase the stack pointer by 1
        inc_stack_pointer = ("@SP\n"
                             "M=M+1\n")

        # Add the two top most values in the stack
        # Set the top most value to 0 and
        # Store the addition on second top most's location
        if command == "add":
            self.out_file.write(get_top_two_stack + 
                                "M=D+M\n" +
                                inc_stack_pointer)

        # Subtract the two top most values in the stack
        # Set the top most value to 0 and
        # Store the subtraction on second top most's location
        elif command == "sub":
            self.out_file.write(get_top_two_stack + 
                                "M=M-D\n" +
                                inc_stack_pointer)

        # Compare the two top most values in the stack
        # Set the top most value to 0 and
        # If second top most is equal to top most
        # Store 1
        # Else store 0
        # On second top most's location
        elif command == "eq":
            self.out_file.write(get_top_two_stack + 
                                "D=D-M\n"
                                "@EQUAL{i}\n"
                                "D;JEQ\n"
                                "@SP\n"
                                "A=M\n"
                                "M=0\n"
                                "@EQUAL_END{i}\n"
                                "0;JMP\n"
                                "(EQUAL{i})\n"
                                "@SP\n"
                                "A=M\n"
                                "M=-1\n"
                                "(EQUAL_END{i})\n".format(i=self.label_counter)
                                + inc_stack_pointer)
            self.label_counter += 1

        # Compare the two top most values in the stack
        # Set the top most value to 0 and
        # If second top most is greater than top most
        # Store 1
        # Else store 0
        # On second top most's location
        elif command == "gt":
            self.out_file.write(get_top_two_stack +
                                "D=M-D\n"
                                "@GT{i}\n"
                                "D;JGT\n"
                                "@SP\n"
                                "A=M\n"
                                "M=0\n"
                                "@GT_END{i}\n"
                                "0;JMP\n"
                                "(GT{i})\n"
                                "@SP\n"
                                "A=M\n"
                                "M=-1\n"
                                "(GT_END{i})\n".format(i=self.label_counter)
                                + inc_stack_pointer)
            self.label_counter += 1

        # Compare the two top most values in the stack
        # Set the top most value to 0 and
        # If second top most is less than top most
        # Store 1
        # Else store 0
        # On second top most's location
        elif command == "lt":
            self.out_file.write(get_top_two_stack +
                                "D=M-D\n"
                                "@LT{i}\n"
                                "D;JLT\n"
                                "@SP\n"
                                "A=M\n"
                                "M=0\n"
                                "@LT_END{i}\n"
                                "0;JMP\n"
                                "(LT{i})\n"
                                "@SP\n"
                                "A=M\n"
                                "M=-1\n"
                                "(LT_END{i})\n".format(i=self.label_counter)
                                + inc_stack_pointer)
            self.label_counter += 1
        
        # Set the NOT value of top most value in stack
        elif command == "not":
            self.out_file.write(get_top_stack +
                                "M=!D\n" +
                                inc_stack_pointer)

        # Set the NEG value of top most value in stack
        elif command == "neg":
            self.out_file.write(get_top_stack +
                                "M=-D\n" +
                                inc_stack_pointer)

        # Operate OR the two top most values in the stack
        # Set the top most value to 0 and
        # Store the result on second top most's location
        elif command == "or":
            self.out_file.write(get_top_two_stack + 
                                "M=D|M\n" +
                                inc_stack_pointer)

        # Operate AND the two top most values in the stack
        # Set the top most value to 0 and
        # Store the result on second top most's location
        elif command == "and":
            self.out_file.write(get_top_two_stack + 
                                "M=D&M\n" +
                                inc_stack_pointer)

    def write_push_pop(self, command, segment, index):
        """writes the assembly code that is
        the translation of the given command

        where command is C_PUSH or C_POP"""

        # Increase the stack pointer by 1
        inc_stack_pointer = ("@SP\n"
                             "M=M+1\n")

        # Decrease the stack pointer by 1
        dec_stack_pointer = ("@SP\n"
                             "M=M-1\n")

        segment_name = {"local": "LCL",
                        "argument": "ARG",
                        "temp": "TEMP", 
                        "pointer": "PTR", 
                        "this": "THIS", 
                        "that": "THAT"}

        if command == "C_PUSH":
            # push constant xxx
            if segment == "constant":
                template = ("@{value}\n"
                           "D=A\n" # store const value in D
                           "@SP\n"
                           "A=M\n" # point to location in SP
                           "M=D\n" + # set it to D
                           inc_stack_pointer) 
                value = index
                self.out_file.write(template.format(
                    value=value))
            elif segment == "static":
                template = ("@{filename}.{index}\n"
                            "D=M\n" # store static value in D
                            "@SP\n"
                            "A=M\n" # point to loc in SP
                            "M=D\n" + # set it to D
                            inc_stack_pointer)
                self.out_file.write(template.format(
                    filename=self.filename,
                    index=index))
            else:
                # push from specified segment index
                # to stack
                template = ("@{index}\n"
                            "D=A\n" # get number of displacement from segment
                            "@{segment}\n"
                            "A=D+M\n"
                            "D=M\n" # segment[index] is now in D
                            "@SP\n"
                            "A=M\n"
                            "M=D\n" + # add value D to top of the stack
                            inc_stack_pointer)
                self.out_file.write(template.format(index=index,
                    segment=segment_name[segment]))

        elif command == "C_POP":
            if segment == "static":
                template = (dec_stack_pointer +
                            "A=M\n"
                            "D=M\n" # topmost stack value now in D
                            "@{filename}.{index}\n"
                            "M=D\n" # save D to static var
                            )
                template = template.format(
                        filename=self.filename,
                        index=index)
                self.out_file.write(template)

            else:
                template = ("@{index}\n"
                            "D=A\n"
                            "@{segment}\n"
                            "D=D+M\n"
                            "@tmp\n" # @tmp stores RAM location to store the value
                            "M=D\n"
                            + dec_stack_pointer +
                            "A=M\n"
                            "D=M\n"
                            "M=0\n"
                            "@tmp\n"
                            "A=M\n" # point to location at @tmp
                            "M=D\n" # set its value to topmost stack value
                            "@tmp\n"
                            "M=0\n" # set @tmp to 0 again
                            )
                self.out_file.write(template.format(
                    segment=segment_name[segment],
                    index=index))

    def close(self):
        """closes opened file"""
        self.out_file.close()

    def init_temp_pointer(self):
        """initialise location for TEMP and PTR"""
        self.out_file.write("@5\n"
                            "D=A\n"
                            "@TEMP\n"
                            "M=D\n")

        self.out_file.write("@3\n"
                            "D=A\n"
                            "@PTR\n"
                            "M=D\n")

    def write_comment(self, comment):
        self.out_file.write("//{comment}\n"
                .format(comment=comment))
