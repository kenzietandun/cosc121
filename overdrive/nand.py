#!/usr/bin/env python3

def NOT(a):
    return not a

def NAND(a, b):
    return not(a and b)

def AND(a, b):
    return a and b

def AND3(a, b, c):
    return AND(AND(a, b), c)

def NOR(a, b):
    return not(a or b)

def OR(a, b):
    return a or b

def OR3(a, b, c):
    return OR(OR(a, b), c)

a = True
b = False
c = True

print(NOT(a))
print(NAND(a, b))
print(AND(a, b))
print(AND3(a, b, c))
print(NOR(a, b))
print(OR(a, b))
print(OR3(a, b, c))
