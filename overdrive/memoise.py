#!/usr/bin/env python3

class memoise:
    def __init__(self, f):
        self.f = f
        self.cache = {}

    def __call__(self, *args):
        if args in self.cache:
            return self.cache[args]
        else:
            self.cache[args] = result = self.f(*args)
            return result

@memoise
def fib(n):
    return 2 if n < 2 else fib(n-1) + fib(n-2)

for i in range(1, 5):
    print("Fib {}".format(i))
    print(fib(i))
